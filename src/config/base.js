import src.config.game as game;
import src.config.menu as menu;

exports = {
    menu: menu,
    game: game,
    maxGameTime: 120,
    objectives: [50, 50, 50, 30, 10],
    bgColor: '#000000',
    rootView: {
        x: 0,
        y: 0,
        width: 320,
        height: 570,
        clip: true
    },
    audioManager: {
        path: 'resources/sounds',
        files: {
            gameLoop: {
                volume: 0.25,
                    background: true,
                    loop: true
            },
            prize: {
                volume: 1,
                    background: false,
                    loop: false
            }
        }
    }
};