exports = {
    gameTitle: {
        x: 0,
        y: 50,
        size: 30,
        verticalAlign: 'middle',
        horizontalAlign: 'center',
        color: '#ffffff',
        text: 'Gem Swapper',
        width: 200,
        height: 100
    },
    playBtnText: {
        x: 0,
        y: 20,
        size: 26,
        verticalAlign: 'middle',
        horizontalAlign: 'center',
        color: '#ffffff',
        text: 'Play',
        width: 200,
        height: 40
    },
    playBtn: {
        x: 160,
        y: 400
    },
    header: {
        x: 160,
        y: 0
    }
};