/*
* Game Data
*
* Used to save and use dynamic data from the game.
* For example:
* - score
* - time
* - activePowerups
*
 */

exports = {
    score: 0,
    totalScore: 0,
    timeRemaining: 0,
    stage: 1,
    numOfGems: 3,
    bestScore: 0,
    tilesCount: [0, 0, 0, 0, 0]
};