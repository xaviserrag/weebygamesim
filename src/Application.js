import src.config.base as config;
import src.gameData as gameData;

import device;
import AudioManager;
import ui.StackView as StackView;
import src.screens.MenuScreen as MenuScreen;
import src.screens.GameScreen as GameScreen;

exports = Class(GC.Application, function () {
    this.initUI = function () {
        var rootViewConfig = config.rootView,
            gameScreen;
        this.view.style.backgroundColor = config.bgColor;

        var baseWidth = rootViewConfig.width; //576
        var baseHeight =  device.screen.height * (rootViewConfig.width / device.screen.width); //864
        var scale = device.screen.width / baseWidth; //1

        var menuScreen = new MenuScreen(),
            rootView = new StackView({
            superview: this,
            x: rootViewConfig.x,
            y: rootViewConfig.y,
            width: rootViewConfig.width,
            height: baseHeight,
            clip: rootViewConfig.clip,
            scale: scale
        });

        var audioManager = new AudioManager(config.audioManager);

        gameData.rootScale = rootView.style.scale;

        audioManager.play('gameLoop');
        rootView.push(menuScreen);

        menuScreen.on('menuScreen:startGame', function () {
            gameScreen = new GameScreen();
            gameScreen.on('gameScreen:endGame', function () {
                rootView.pop();
            });
            rootView.push(gameScreen);
        });


  };

  this.launchUI = function () {
  };
});
