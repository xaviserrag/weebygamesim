import src.config.base as config;
import src.gameData as gameData;
import src.screens.GameOverPopup as GameOverPopup;
import src.screens.WinPopup as WinPopup;
import src.screens.UserInterface as UI;

import device;
import animate;
import AudioManager;
import ui.View as View;
import ui.resource.Image as Image;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import event.input.drag as drag;
import ui.ParticleEngine as ParticleEngine;

/*
*
*This function has a private scope
*
 */
var getRandom = function getRandom(from, to) {
    return Math.floor((Math.random() * to) + from);
};

exports = Class(ImageView, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			image: "resources/images/menu_bg.png"
		});

		supr(this, 'init', [opts]);
		this.build();
	};

	this.build = function() {
        var self = this,
            initialMatches,
            audioManager = new AudioManager(config.game.audio),
            tiles = [
                new Image({url: "resources/images/gems/gem_01.png"}),
                new Image({url: "resources/images/gems/gem_02.png"}),
                new Image({url: "resources/images/gems/gem_03.png"}),
                new Image({url: "resources/images/gems/gem_04.png"}),
                new Image({url: "resources/images/gems/gem_05.png"})
            ],
            particles = [
                "resources/images/particles/gleam_purple.png",
                "resources/images/particles/gleam_yellow.png",
                "resources/images/particles/gleam_blue.png",
                "resources/images/particles/gleam_red.png",
                "resources/images/particles/gleam_green.png",
                "resources/images/particles/gleam_white.png"
            ],
            roundParticles = [
                "resources/images/particles/round_purple.png",
                "resources/images/particles/round_yellow.png",
                "resources/images/particles/round_blue.png",
                "resources/images/particles/round_red.png",
                "resources/images/particles/round_green.png",
                "resources/images/particles/round_white.png"
            ],
            isGameOver = false,         //Boolean used to detect if the game is over and the user lost.
            isWin = false,              //Boolean used to detect if the game is over and the user won.
            alreadySwapped = false;     //Boolean used to detect if the incorrect swap already swapped back.

        this.dragging = false;           //Boolean used to detect if the game is in swapping process, public because the
                                         //UI uses it from outside this context.

        /*
         * Function that swaps the tiles, then check if a prize is generated,
         * if not, swap again to the start position, if prize matched, start
         * deleting process.
         */
        var swapTiles = function swapTiles(sourceTile, targetTile) {
            var sourcePos = {x: sourceTile.style.x, y: sourceTile.style.y, col: sourceTile.col, row: sourceTile.row},
                targetPos = {x: targetTile.style.x, y: targetTile.style.y, col: targetTile.col, row: targetTile.row};

            var checkPrize = function checkPrize() {
                var matches = checkMatches();

                if (matches.length > 0) {
                    alreadySwapped = true;
                    deleteTiles(matches, true);
                } else {
                    self.dragging = false;
                    if (!alreadySwapped) {
                        alreadySwapped = true;
                        swapTiles(targetTile, sourceTile);
                    }
                }
            };

            self.dragging = true;

            sourceTile.col = targetPos.col;
            sourceTile.row = targetPos.row;

            targetTile.col = sourcePos.col;
            targetTile.row = sourcePos.row;

            self.grid[sourcePos.row][sourcePos.col] = targetTile;
            self.grid[targetPos.row][targetPos.col] = sourceTile;

            animate(targetTile).now({x: sourcePos.x, y: sourcePos.y}, 300, animate.easeInOut);
            animate(sourceTile).now({x: targetPos.x, y: targetPos.y}, 300, animate.easeInOut)
                .then(checkPrize);
        };

        /*
         * Function that set callbacks to the drag event.
         */
        var setDraggableEvents = function setDraggableEvents(item, itemImg) {
            var moveLeft, moveRight, moveUp, moveDown, pointerPosX, pointerPosY, tileWidth = 48, tileHeight = 48;

            item.on("InputStart", function (evt) {
                if (!self.dragging && !isGameOver && !isWin) {
                    item.startDrag({
                        inputStartEvt: evt,
                        radius: 10
                    });
                    alreadySwapped = false;
                    item.alreadyDragged = false;
                }

            });

            item.on('Drag', function (startEvt, dragEvt) {
                if (item.alreadyDragged) {return;}

                pointerPosX = dragEvt.srcPt.x / gameData.rootScale;
                pointerPosY = dragEvt.srcPt.y / gameData.rootScale;

                moveRight = item.style.x + (tileWidth/2) + tileWidth <= pointerPosX && self.grid[item.row][item.col + 1];
                moveLeft = item.style.x + (tileWidth/2) - tileWidth >= pointerPosX  && self.grid[item.row][item.col - 1];
                moveUp = item.style.y + (tileHeight/2) - tileHeight >= pointerPosY  && self.grid[item.row - 1];
                moveDown = item.style.y + (tileWidth/2) + tileWidth <= pointerPosY  && self.grid[item.row + 1];

                if (moveRight || moveLeft || moveUp || moveDown) {
                    item.alreadyDragged = true;

                    if(moveRight) {
                        swapTiles(item, self.grid[item.row][item.col + 1]);
                    } else if (moveLeft) {
                        swapTiles(item, self.grid[item.row][item.col - 1]);
                    } else if (moveUp) {
                        swapTiles(item, self.grid[item.row - 1][item.col]);
                    } else if (moveDown) {
                        swapTiles(item, self.grid[item.row + 1][item.col]);
                    }
                }
            });
        };

        /*
         * Function that checks the matches on the grid, with a minimum of 3 same tiles to have a match (horizontally
         * and vertically).
         */
        var checkMatches = function checkMatches () {
            var grid = self.grid,
                resultMatches;


            var checkRowMatches = function checkRowMatches() {
                var matches = [];
                for (var i = 0, len = grid.length; i < len; i++) {
                    var rowMatches = [];
                    for (var j = 0, len2 = grid[i].length; j < len2; j++) {
                        if (j < grid[i].length - 2) {
                            if (grid[i][j].type === grid[i][j + 1].type && grid[i][j].type === grid[i][j + 2].type) {
                                if (rowMatches.length > 0) {
                                    if (rowMatches.indexOf(grid[i][j]) == -1) {
                                        matches.push(rowMatches);
                                        rowMatches = [];
                                    } else {
                                        rowMatches.push(grid[i][j + 2]);
                                    }
                                } else {
                                    rowMatches.push(grid[i][j]);
                                    rowMatches.push(grid[i][j + 1]);
                                    rowMatches.push(grid[i][j + 2]);
                                }
                            }
                        }
                    }

                    if (rowMatches.length > 0) {
                        matches.push(rowMatches);
                    }
                }

                return matches;
            };

            var checkColMatches = function checkColMatches() {
                var matches = [];

                for (var j = 0, len = grid.length; j < len; j++) {
                    var colMatches = [];
                    for (var i = 0, len2 = grid[j].length; i < len2; i++) {
                        if (i < grid[j].length - 2) {
                            if (grid[i][j].type === grid[i + 1][j].type && grid[i][j].type === grid[i + 2][j].type) {
                                if (colMatches.length > 0) {
                                    if (colMatches.indexOf(grid[i][j]) == -1) {
                                        matches.push(colMatches);
                                        colMatches = [];
                                    } else {
                                        colMatches.push(grid[i + 2][j]);
                                    }
                                } else {
                                    colMatches.push(grid[i][j]);
                                    colMatches.push(grid[i + 1][j]);
                                    colMatches.push(grid[i + 2][j]);
                                }
                            }
                        }
                    }
                    if (colMatches.length > 0) {
                        matches.push(colMatches);
                    }
                }

                return matches;
            };

            resultMatches = checkRowMatches();
            resultMatches = resultMatches.concat(checkColMatches());

            return resultMatches;
        };

        /*
         * Function that checks if the level objectives are completed and shows the winGamePopup.
         */
        var checkObjectiveCompletion = function checkObjectiveCompletion() {
            var completed = gameData.tilesCount.every(function(item, index) {
                return item >= config.objectives[index];
            });

            !self.dragging && completed && winGame();
        };

        /*
         * Function that increase the score, level and num of different tiles.
         */
        var increaseScore = function increaseScore(tile) {
            gameData.score += config.game.scoreStep;

            if(gameData.tilesCount[tile.type] < config.objectives[tile.type]){
                gameData.tilesCount[tile.type]++;
            }

            self.tileObjectivesText[tile.type].setText(gameData.tilesCount[tile.type] + ' / ' + config.objectives[tile.type]);

            if (gameData.score % 100 === 0) {
                gameData.stage++;
                self.stageNum.setText(gameData.stage);

                animate(self.stageContainer).now({scale: 1.3}, 500, animate.easeInOut)
                    .then({scale: 1}, 500, animate.easeInOut);

                if (gameData.stage % config.game.gemStep === 0 && gameData.stage <= config.game.topGemIncreaseLevel) {
                    gameData.numOfGems++;
                }
            }

            self.scoreNum.setText(gameData.score);
        };

        /*
         * Function that starts creating particles.
         */
        var emitParticles = function emitParticles(tile) {
            var particleObjects = self.pEngine.obtainParticleArray(10),
                particleTypes, randomNum, randomNum2;

            for (var i = 0; i < 10; i++) {
                randomNum = getRandom(0,2),
                randomNum2 = getRandom(0,2);

                particleTypes = randomNum2 ? roundParticles : particles;
                pObj = particleObjects[i];
                pObj.x = tile.style.x + 15;
                pObj.y = tile.style.y + 15;
                pObj.dx = randomNum2 === 1 ? -Math.random() * 17 : Math.random() * 17;
                pObj.dy = randomNum2 === 1 ? -Math.random() * 17 : Math.random() * 17;
                pObj.ttl = 1200;
                pObj.width = 22;
                pObj.height = 22;
                pObj.image = randomNum === 1 ? particleTypes[tile.type] : particles[5];
            }

            self.pEngine.emitParticles(particleObjects);
        };

        /*
         * Function that starts the delete process.
         *  - Delete.
         *  - Move everything down.
         *  - Create new tiles.
         *  - Delete again if there are more mathces.
         */
        var deleteTiles = function deleteTiles(matches, animationFlag) {
            var timeoutSpeed = animationFlag ? 700 : 1,
                timeoutSpeed2 = animationFlag ? 150 : 1,
                timeoutSpeed3 = animationFlag ? 500 : 1;

            animationFlag && audioManager.play('prize');

            /*
             * Tile deleting process.
             */
            matches.forEach(function(group) {
                    if(group.length > 0) {
                        group.forEach(function (tile) {
                            self.grid[tile.row][tile.col] = null;
                            if (animationFlag) {
                                increaseScore(tile);
                                emitParticles(tile);

                                animate(tile).now({opacity: 0}, 200, animate.easeInOut);

                                setTimeout(function(){
                                    tile.removeFromSuperview();
                                }, 450);

                            } else {
                                tile.removeFromSuperview();
                            }
                        });
                    }
            });


            /*
             * Chain of timeouts to set all the delete process flow.
             *  - First: waits until the particles are dissapearing (500ms).
             *  - Second: waits until all the tiles are positioned correctly with the moveAllTilesDown method (150ms).
             *  - Third: waits until all the empty spaces are filled and the animation of the tiles falling is finished (700ms).
             *  - Finally when the tiles are on their position, new matches are checked, and if there is more prizes,
             *  deleteTiles is called again, and the flow is done again until there is no more matches.
             */
            setTimeout(function(){
                moveAllTilesDown(animationFlag);
                setTimeout(function() {
                    fillEmptyTiles(animationFlag);
                    setTimeout(function(){
                        var matches = checkMatches(self.grid);

                        if(matches.length > 0) {
                            deleteTiles(matches, animationFlag);
                        } else {
                            self.dragging = false;
                            checkObjectiveCompletion();
                            gameData.timeRemaining === 0 && !isWin && gameOver();
                        }
                    }, timeoutSpeed);
                }, timeoutSpeed2);
            }, timeoutSpeed3);

        };

        /*
         * Function that search empty spaces and fill them with new tiles.
         * If animation is activated the tiles are placed over the canvas, and animated down
         * to the original position.
         */
        var fillEmptyTiles = function fillEmptyTiles(animationFlag) {
            var tile, randomNum, finalY;

            self.grid.forEach(function(row, i) {
                row.forEach(function(item, j) {
                    if (item == null) {
                        randomNum = getRandom(0, gameData.numOfGems);
                        tile = createTile({row: i, col: j, type: randomNum});
                        self.grid[i][j] = tile;

                        if(animationFlag) {
                            finalY = tile.style.y;
                            tile.style.y  = -100;
                            animate(tile).now({y: finalY}, 600, animate.easeOutBounce);
                        }
                    }
                })
            });
        };

        /*
         * Function that move all the tiles down after deleting the prized tiles.
         */
        var moveAllTilesDown = function moveAllTilesDown(animationFlag) {

            /*
            * After the loop the finalY property of the tile is the last position that the tile will move, so the
            * target of the animation is the finalY property.
            */
            var animateTiles = function animateTiles() {
                self.grid.forEach(function (group) {
                    group.forEach(function (tile) {
                        if (tile && tile.finalY !== 0) {
                            animate(tile).now({y: tile.style.y + tile.finalY}, 300, animate.easeOutBounce);
                            tile.finalY = 0;
                        }
                    });
                });

            };

            /*
            * reversed loop to iterate from the bottom rows to the top ones.
            */
            for (var i = self.grid.length - 1; i > 0; i--) {
                var shouldRepeat = false;
                for (var j = 0; j < self.grid[i].length; j++) {
                    if(self.grid[i][j] === null && self.grid[i - 1][j] !== null) {
                        self.grid[i][j] = self.grid[i - 1][j];
                        self.grid[i - 1][j] = null;
                        self.grid[i][j].row++;

                        /*
                        * If animations are activated, the loop just saves a finalY property on the tile and increase it
                        * every iteration, then the animation is fluid from the original position to the last one.
                        */
                        if (animationFlag) {
                            self.grid[i][j].finalY += (tiles[0].getHeight() * config.game.grid.tilesScale);
                        } else {
                            self.grid[i][j].style.y += (tiles[0].getHeight() * config.game.grid.tilesScale);
                        }
                        shouldRepeat = true;
                    }
                }

                /*
                 * If an empty tile is found, the row needs to be iterated again. With this the loop will iterate until all the
                 * tiles are placed correctly on the last bottom position possible.
                 */
                if (shouldRepeat) i = self.grid.length;
            }

            animationFlag && animateTiles();
        };

        /*
         * Function that reset the game data
         */
        var resetGameData = function resetGameData() {
            gameData.score = 0;
            gameData.totalScore = 0;
            gameData.stage = 1;
            gameData.tilesCount = [0, 0, 0, 0, 0];
            gameData.numOfGems = config.game.grid.starterNumOfGems;
        };

        /*
         * Function that ends the game, reseting the game data and
         * emiting the endGame signal.
         *
         * (Also clears the timer interval that is created at the start of the game)
         */
        var endGame = function endGame() {
            self.once('InputSelect', function () {
                resetGameData();
                self.emit('gameScreen:endGame');
            });
            clearInterval(self.timerInterval);
        };

        /*
         * Function that set the win state and the final Score
         */
        var winGame = function winGame() {
            var isBestScore;
            isWin = true;
            gameData.totalScore = gameData.score + (gameData.timeRemaining * config.game.timeMultiplier);

            isBestScore = gameData.totalScore > gameData.bestScore

            if (isBestScore) {
                gameData.bestScore = gameData.totalScore;
            }

            endGame();
            createWinPopup(isBestScore);
        };

        /*
         * Function that set the gameOver state.
         */
        var gameOver = function gameOver() {
            isGameOver = true;
            endGame();
            createGameOverPopup();
        };

        /*
         * Function that generates a tile.
         * Sets the tile draggable and add it to the view
         */
        var createTile = function createTile(params) {
            var tile = new ImageView ({
                x: config.game.grid.offset.x + tiles[params.type].getWidth() * config.game.grid.tilesScale * params.col,
                y: config.game.grid.offset.y + tiles[params.type].getHeight() * config.game.grid.tilesScale * params.row,
                width: tiles[params.type].getWidth(),
                height: tiles[params.type].getHeight(),
                image: tiles[params.type],
                scale: config.game.grid.tilesScale,
                centerAnchor: true
            });

            tile.row = params.row;
            tile.col = params.col;
            tile.type = params.type;
            tile.finalY = 0;

            setDraggableEvents(tile, tiles[params.type]);

            self.addSubview(tile);

            return tile;
        };

        /*
         * Function that generates the initial grid of tiles, only called at the start of the game.
         */
        var generateGrid = function generateGrid() {
            var grid = [], randomNum;

            for (var i = 0; i < config.game.grid.maxRow; i++) {
                grid[i] = [];
                for (var j = 0; j < config.game.grid.maxCol; j++) {
                    randomNum = getRandom(0, gameData.numOfGems);
                    grid[i][j] = createTile({row: i, col: j, type: randomNum});
                }
            }

            return grid;
        };

        /*
         * Function to create the game over popup, only created if loosing, not at the start.
         */
        var createGameOverPopup = function createGameOverPopup() {
            self.gameOverPopup = new GameOverPopup(self);
        };

        /*
         * Function to create the winning popup, only created if winning, not at the start.
         */
        var createWinPopup = function createWinPopup(newBestScore) {
            self.winPopup = new WinPopup(self, newBestScore);
        };

        /*
         * Function to set up the particle engine and add the runTick callback to the tick event
         */
        var setupParticleEngine = function() {
            self.pEngine = new ParticleEngine({
                superview: self,
                width: 1,
                height: 1,
                initCount: 10
            });

            self.tick = function(dt) {
                self.pEngine.runTick(dt);
            };

            GC.app.engine.on('Tick', function (dt) {
                self.tick(dt);
            });
        };

        /*
         * Create the grid and the UI.
         */
        this.grid = generateGrid();
        self.ui = new UI(self, tiles, gameOver);

        /*
        * Create the particleEngine.
        */
        setupParticleEngine();

        /*
        * Clean the grid of random prizes before the game starts.
        */
        initialMatches = checkMatches(self.grid);
        initialMatches.length > 0 && deleteTiles(initialMatches);
    };
});
