import src.config.base as config;
import src.gameData as gameData;
import animate;

import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

var UserInterface = function UserInterface(parentView, tiles, gameOver) {
    var createLevel = function createLevel() {
        parentView.stageContainer = new View({
            superview: parentView,
            x: config.game.stageContainer.x,
            y: config.game.stageContainer.y,
            width: config.game.stageContainer.width,
            height: config.game.stageContainer.height
        });

        parentView.stageTxt = new TextView({
            superview: parentView.stageContainer,
            size: config.game.stageTxt.size,
            verticalAlign: config.game.stageTxt.verticalAlign,
            horizontalAlign: config.game.stageTxt.horizontalAlign,
            text: config.game.stageTxt.text,
            color: config.game.stageTxt.color,
            x: config.game.stageTxt.x,
            y: config.game.stageTxt.y,
            width: config.game.stageTxt.width,
            height: config.game.stageTxt.height
        });

        parentView.stageNum = new TextView({
            superview: parentView.stageContainer,
            size: config.game.stageNum.size,
            verticalAlign: config.game.stageNum.verticalAlign,
            horizontalAlign: config.game.stageNum.horizontalAlign,
            text: config.game.stageNum.text,
            color: config.game.stageNum.color,
            x: config.game.stageNum.x,
            y: config.game.stageNum.y,
            width: config.game.stageNum.width,
            height: config.game.stageNum.height
        });
    };

    var createScore = function createScore() {
        parentView.scoreTxt = new TextView({
            superview: parentView,
            size: config.game.scoreTxt.size,
            verticalAlign: config.game.scoreTxt.verticalAlign,
            horizontalAlign: config.game.scoreTxt.horizontalAlign,
            text: config.game.scoreTxt.text,
            color: config.game.scoreTxt.color,
            x: config.game.scoreTxt.x,
            y: config.game.scoreTxt.y,
            width: config.game.scoreTxt.width,
            height: config.game.scoreTxt.height
        });

        parentView.scoreNum = new TextView({
            superview: parentView,
            size: config.game.scoreNum.size,
            verticalAlign: config.game.scoreNum.verticalAlign,
            horizontalAlign: config.game.scoreNum.horizontalAlign,
            text: config.game.scoreNum.text,
            color: config.game.scoreNum.color,
            x: config.game.scoreNum.x,
            y: config.game.scoreNum.y,
            width: config.game.scoreNum.width,
            height: config.game.scoreNum.height
        });
    };

    var createTimer = function createTimer() {
        parentView.timerTxt = new TextView({
            superview: parentView,
            size: config.game.timerTxt.size,
            verticalAlign: config.game.timerTxt.verticalAlign,
            horizontalAlign: config.game.timerTxt.horizontalAlign,
            text: config.game.timerTxt.text,
            color: config.game.timerTxt.color,
            x: config.game.timerTxt.x,
            y: config.game.timerTxt.y,
            width: config.game.timerTxt.width,
            height: config.game.timerTxt.height
        });

        parentView.timerNum = new TextView({
            superview: parentView,
            size: config.game.timerNum.size,
            verticalAlign: config.game.timerNum.verticalAlign,
            horizontalAlign: config.game.timerNum.horizontalAlign,
            text: config.game.timerNum.text,
            color: config.game.timerNum.color,
            x: config.game.timerNum.x,
            y: config.game.timerNum.y,
            width: config.game.timerNum.width,
            height: config.game.timerNum.height
        });

        gameData.timeRemaining = config.maxGameTime;
        parentView.timerInterval = setInterval(function () {
            if(gameData.timeRemaining > 0) {
                gameData.timeRemaining--;
                parentView.timerNum.setText(gameData.timeRemaining);
            } else {
                !parentView.dragging && gameOver();
            }
        }, 1000);
    };

    var createObjectives = function createObjectives() {
        var objConf = config.game.objectives;

        var blueTile = new ImageView({
            supeview: parentView,
            x: objConf.blueTile.x,
            y: objConf.blueTile.y,
            width: tiles[2].getWidth(),
            height: tiles[2].getHeight(),
            image: tiles[2],
            scale: objConf.blueTile.scale
        });

        var blueTileTxt = new TextView({
            superview: parentView,
            size: objConf.blueTileTxt.size,
            verticalAlign: objConf.blueTileTxt.verticalAlign,
            horizontalAlign: objConf.blueTileTxt.horizontalAlign,
            text: gameData.tilesCount[2] + ' / ' + config.objectives[2],
            color: objConf.blueTileTxt.color,
            x: objConf.blueTileTxt.x,
            y: objConf.blueTileTxt.y,
            width: objConf.blueTileTxt.width,
            height: objConf.blueTileTxt.height
        });

        var purpleTile = new ImageView({
            supeview: parentView,
            x: objConf.purpleTile.x,
            y: objConf.purpleTile.y,
            width: tiles[0].getWidth(),
            height: tiles[0].getHeight(),
            image: tiles[0],
            scale: objConf.purpleTile.scale
        });

        var purpleTileTxt = new TextView({
            superview: parentView,
            size: objConf.purpleTileTxt.size,
            verticalAlign: objConf.purpleTileTxt.verticalAlign,
            horizontalAlign: objConf.purpleTileTxt.horizontalAlign,
            text: gameData.tilesCount[0] + ' / ' + config.objectives[0],
            color: '#000000',
            x: objConf.purpleTileTxt.x,
            y: objConf.purpleTileTxt.y,
            width: objConf.purpleTileTxt.width,
            height: objConf.purpleTileTxt.height
        });

        var yellowTile = new ImageView({
            supeview: parentView,
            x: objConf.yellowTile.x,
            y: objConf.yellowTile.y,
            width: tiles[1].getWidth(),
            height: tiles[1].getHeight(),
            image: tiles[1],
            scale: objConf.yellowTile.scale
        });

        var yellowTileTxt = new TextView({
            superview: parentView,
            size: objConf.yellowTileTxt.size,
            verticalAlign: objConf.yellowTileTxt.verticalAlign,
            horizontalAlign: objConf.yellowTileTxt.horizontalAlign,
            text: gameData.tilesCount[1] + ' / ' + config.objectives[1],
            color: objConf.yellowTileTxt.color,
            x: objConf.yellowTileTxt.x,
            y: objConf.yellowTileTxt.y,
            width: objConf.yellowTileTxt.width,
            height: objConf.yellowTileTxt.height
        });

        var redTile = new ImageView({
            supeview: parentView,
            x: objConf.redTile.x,
            y: objConf.redTile.y,
            width: tiles[3].getWidth(),
            height: tiles[3].getHeight(),
            image: tiles[3],
            scale: objConf.redTile.scale
        });

        var redTileTxt = new TextView({
            superview: parentView,
            size: objConf.redTileTxt.size,
            verticalAlign: objConf.redTileTxt.verticalAlign,
            horizontalAlign: objConf.redTileTxt.horizontalAlign,
            text: gameData.tilesCount[3] + ' / ' + config.objectives[3],
            color: objConf.redTileTxt.color,
            x: objConf.redTileTxt.x,
            y: objConf.redTileTxt.y,
            width: objConf.redTileTxt.width,
            height: objConf.redTileTxt.height
        });

        var greenTile = new ImageView({
            supeview: parentView,
            x: objConf.greenTile.x,
            y: objConf.greenTile.y,
            width: tiles[4].getWidth(),
            height: tiles[4].getHeight(),
            image: tiles[4],
            scale: objConf.greenTile.scale
        });

        var greenTileTxt = new TextView({
            superview: parentView,
            size: objConf.greenTileTxt.size,
            verticalAlign: objConf.greenTileTxt.verticalAlign,
            horizontalAlign: objConf.greenTileTxt.horizontalAlign,
            text: gameData.tilesCount[4] + ' / ' + config.objectives[4],
            color: objConf.greenTileTxt.color,
            x: objConf.greenTileTxt.x,
            y: objConf.greenTileTxt.y,
            width: objConf.greenTileTxt.width,
            height: objConf.greenTileTxt.height
        });

        parentView.addSubview(blueTile);
        parentView.addSubview(purpleTile);
        parentView.addSubview(yellowTile);
        parentView.addSubview(redTile);
        parentView.addSubview(greenTile);

        parentView.tileObjectivesText = [
            purpleTileTxt,
            yellowTileTxt,
            blueTileTxt,
            redTileTxt,
            greenTileTxt
        ];
    };

    createLevel();
    createScore();
    createTimer();
    createObjectives();
};

exports = UserInterface;
