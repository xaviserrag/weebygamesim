import src.config.base as config;
import animate;

import ui.View;
import ui.resource.Image as Image;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

exports = Class(ImageView, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			image: "resources/images/menu_bg.png"
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	this.build = function() {
        var self = this;

        var titleConfig = config.menu.gameTitle,
            headerConfig = config.menu.header,
            playBtnConfig = config.menu.playBtn,
            playBtnTextConfig = config.menu.playBtnText,
            headerImg = new Image({url: "resources/images/header.png"}),
            playBtnImg = new Image({url: "resources/images/play_bg.png"});

        var header = new ImageView({
            supeview: this,
            x: headerConfig.x - headerImg.getWidth()/2,
            y: headerConfig.y - headerImg.getHeight(),
            width: headerImg.getWidth(),
            height: headerImg.getHeight(),
            image: 'resources/images/header.png'
        });

        var playBtn = new ImageView({
            supeview: this,
            x: playBtnConfig.x - playBtnImg.getWidth()/2,
            y: playBtnConfig.y - playBtnImg.getHeight(),
            width: playBtnImg.getWidth(),
            height: playBtnImg.getHeight(),
            image: playBtnImg,
            opacity: 0
        });

        var title = new TextView({
            superview: header,
            size: titleConfig.size,
            verticalAlign: titleConfig.verticalAlign,
            horizontalAlign: titleConfig.horizontalAlign,
            text: titleConfig.text,
            color: titleConfig.color,
            x: titleConfig.x,
            y: titleConfig.y,
            width: headerImg.getWidth(),
            height: titleConfig.height
        });

        var playBtnText = new TextView({
            superview: playBtn,
            size: playBtnTextConfig.size,
            verticalAlign: playBtnTextConfig.verticalAlign,
            horizontalAlign: playBtnTextConfig.horizontalAlign,
            text: playBtnTextConfig.text,
            color: playBtnTextConfig.color,
            x: playBtnTextConfig.x,
            y: playBtnTextConfig.y,
            width: playBtnImg.getWidth(),
            height: playBtnTextConfig.height
        });

        this.addSubview(header);
        this.addSubview(playBtn);

        var playBtnCallback = function playBtnCallback () {
            self.emit('menuScreen:startGame');
        };

        playBtn.on('InputSelect', playBtnCallback);


        var animateBtn = function animateBtn () {
            animate(playBtn).now({opacity: 1}, 700, animate.easeInOut);
        };

        animate(header).now({y: headerConfig.y}, 800, animate.easeOutBounce)
            .then(animateBtn);

    };
});
