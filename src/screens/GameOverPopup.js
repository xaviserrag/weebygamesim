import src.config.base as config;
import animate;

import ui.View as View;
import ui.TextView as TextView;

var GameOver = function GameOver(parentView) {
    this.gameOverContainer = new View({
        superview: parentView,
        x: 0,
        y: 0,
        opacity: 0,
        width: config.rootView.width,
        height: config.rootView.height,
        backgroundColor: "rgba(0, 0, 0, 0.7)"
    });

    this.gameOverTxt = new TextView({
        superview: this.gameOverContainer,
        size: config.game.gameOverTxt.size,
        x: config.game.gameOverTxt.x,
        y: config.game.gameOverTxt.y,
        verticalAlign: config.game.gameOverTxt.verticalAlign,
        horizontalAlign: config.game.gameOverTxt.horizontalAlign,
        text: config.game.gameOverTxt.text,
        color: config.game.gameOverTxt.color,
        width: config.game.gameOverTxt.width,
        height: config.game.gameOverTxt.height
    });

    this.gameOverDescriptionTxt = new TextView({
        superview: this.gameOverContainer,
        size: config.game.gameOverDescriptionTxt.size,
        x: config.game.gameOverDescriptionTxt.x,
        y: config.game.gameOverDescriptionTxt.y,
        verticalAlign: config.game.gameOverDescriptionTxt.verticalAlign,
        horizontalAlign: config.game.gameOverDescriptionTxt.horizontalAlign,
        text: config.game.gameOverDescriptionTxt.text,
        color: config.game.gameOverDescriptionTxt.color,
        width: config.game.gameOverDescriptionTxt.width,
        height: config.game.gameOverDescriptionTxt.height
    });

    this.gameOverClickTxt = new TextView({
        superview: this.gameOverContainer,
        size: config.game.gameOverClickTxt.size,
        x: config.game.gameOverClickTxt.x,
        y: config.game.gameOverClickTxt.y,
        verticalAlign: config.game.gameOverClickTxt.verticalAlign,
        horizontalAlign: config.game.gameOverClickTxt.horizontalAlign,
        text: config.game.gameOverClickTxt.text,
        color: config.game.gameOverClickTxt.color,
        width: config.game.gameOverClickTxt.width,
        height: config.game.gameOverClickTxt.height
    });

    animate(this.gameOverContainer).now({opacity: 1}, 500, animate.easeInOut)
};

exports = GameOver;
