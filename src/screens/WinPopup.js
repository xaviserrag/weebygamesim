import src.config.base as config;
import src.gameData as gameData;

import animate;
import ui.View as View;
import ui.TextView as TextView;

var Win = function Win(parentView, newBestScore) {
    self.winContainer = new View({
        superview: parentView,
        x: 0,
        y: 0,
        opacity: 0,
        width: config.rootView.width,
        height: config.rootView.height,
        backgroundColor: "rgba(0, 0, 0, 0.7)"
    });

    self.winTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winTxt.size,
        x: config.game.winTxt.x,
        y: config.game.winTxt.y,
        verticalAlign: config.game.winTxt.verticalAlign,
        horizontalAlign: config.game.winTxt.horizontalAlign,
        text: config.game.winTxt.text,
        color: config.game.winTxt.color,
        width: config.game.winTxt.width,
        height: config.game.winTxt.height
    });

    self.winTimeLeftTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winTimeLeftTxt.size,
        x: config.game.winTimeLeftTxt.x,
        y: config.game.winTimeLeftTxt.y,
        verticalAlign: config.game.winTimeLeftTxt.verticalAlign,
        horizontalAlign: config.game.winTimeLeftTxt.horizontalAlign,
        text: config.game.winTimeLeftTxt.text + ' ' + (gameData.timeRemaining * config.game.timeMultiplier),
        color: config.game.winTimeLeftTxt.color,
        width: config.game.winTimeLeftTxt.width,
        height: config.game.winTimeLeftTxt.height,
        opacity: 0
    });

    self.winScoreTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winScoreTxt.size,
        x: config.game.winScoreTxt.x,
        y: config.game.winScoreTxt.y,
        verticalAlign: config.game.winScoreTxt.verticalAlign,
        horizontalAlign: config.game.winScoreTxt.horizontalAlign,
        text: config.game.winScoreTxt.text + ' ' + gameData.score,
        color: config.game.winScoreTxt.color,
        width: config.game.winScoreTxt.width,
        height: config.game.winScoreTxt.height,
        opacity: 0
    });

    self.winTotalScoreTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winTotalScoreTxt.size,
        x: config.game.winTotalScoreTxt.x,
        y: config.game.winTotalScoreTxt.y,
        verticalAlign: config.game.winTotalScoreTxt.verticalAlign,
        horizontalAlign: config.game.winTotalScoreTxt.horizontalAlign,
        text: config.game.winTotalScoreTxt.text + ' ' + (gameData.totalScore),
        color: config.game.winTotalScoreTxt.color,
        width: config.game.winTotalScoreTxt.width,
        height: config.game.winTotalScoreTxt.height,
        opacity: 0
    });

    self.winBestScoreTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winBestScoreTxt.size,
        x: config.game.winBestScoreTxt.x,
        y: config.game.winBestScoreTxt.y,
        verticalAlign: config.game.winBestScoreTxt.verticalAlign,
        horizontalAlign: config.game.winBestScoreTxt.horizontalAlign,
        text: config.game.winBestScoreTxt.text + ' ' + gameData.bestScore,
        color: config.game.winBestScoreTxt.color,
        width: config.game.winBestScoreTxt.width,
        height: config.game.winBestScoreTxt.height,
        opacity: 0
    });

    self.winClickTxt = new TextView({
        superview: self.winContainer,
        size: config.game.winClickTxt.size,
        x: config.game.winClickTxt.x,
        y: config.game.winClickTxt.y,
        verticalAlign: config.game.winClickTxt.verticalAlign,
        horizontalAlign: config.game.winClickTxt.horizontalAlign,
        text: config.game.winClickTxt.text,
        color: config.game.winClickTxt.color,
        width: config.game.winClickTxt.width,
        height: config.game.winClickTxt.height,
        opacity: 0
    });

    if (newBestScore) {
        self.winNewBestTxt = new TextView({
            superview: self.winContainer,
            size: config.game.winNewBestTxt.size,
            x: config.game.winNewBestTxt.x,
            y: config.game.winNewBestTxt.y,
            verticalAlign: config.game.winNewBestTxt.verticalAlign,
            horizontalAlign: config.game.winNewBestTxt.horizontalAlign,
            text: config.game.winNewBestTxt.text,
            color: config.game.winNewBestTxt.color,
            width: config.game.winNewBestTxt.width,
            height: config.game.winNewBestTxt.height,
            opacity: 0
        });
    }

    animate(self.winContainer).now({opacity: 1}, 500, animate.easeInOut)
        .wait(500)
        .then(function() {
            animate(self.winTimeLeftTxt).now({opacity: 1}, 20, animate.easeInOut)
                .wait(500)
                .then(function() {
                    animate(self.winScoreTxt).now({opacity: 1}, 20, animate.easeInOut)
                        .wait(500)
                        .then(function() {
                            animate(self.winTotalScoreTxt).now({opacity: 1}, 20, animate.easeInOut)
                                .wait(500)
                                .then(function() {
                                    animate(self.winBestScoreTxt).now({opacity: 1}, 20, animate.easeInOut)
                                        .wait(500)
                                        .then(function() {
                                            newBestScore && animate(self.winNewBestTxt).now({opacity: 1}, 20, animate.easeInOut);
                                            animate(self.winClickTxt).now({opacity: 1}, 20, animate.easeInOut);
                                        })
                                })
                        })
                })
        })
};

exports = Win;
